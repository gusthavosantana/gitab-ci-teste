# Changelog

All notable changes to this project will be documented in this file.

# [1.1.0](https://gitlab.com/gusthavosantana/gitab-ci-teste/compare/v1.0.20...v1.1.0) (2023-05-08)


### Features

* nova feature ([067ab1d](https://gitlab.com/gusthavosantana/gitab-ci-teste/commit/067ab1d7c2358bee424dbb17e6f7888296c4342e))

## [1.0.20](https://gitlab.com/gusthavosantana/gitab-ci-teste/compare/v1.0.19...v1.0.20) (2023-05-08)


### Bug Fixes

* release config ([6965599](https://gitlab.com/gusthavosantana/gitab-ci-teste/commit/6965599f33d9bd6cceae8c0096ec26e2a45ae140))
* release config ([2ed53ff](https://gitlab.com/gusthavosantana/gitab-ci-teste/commit/2ed53ff7e6fc9ee434d0f5707684925c03680588))
